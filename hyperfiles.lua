hexchat.register('hyperfiles', 8, '')
hexchat.print('[hyperfiles] starting')

-- TODOS
-- BOT MULTIPLI
-- cerca contesto con nome room giusta e usa nome bot invece di scrivere comando intero in file input
-- hexchat.iterate(dcc)
-- timer -> text event?
-- extra layer sicurezza per limitare comandi/secondo verso server
-- array -> stack/queue
-- logic->internal event->log, log class
-- timeouts? accept+queue failure, download (se speed < min kb), download aborted
-- reload input file to add downloads
-- automatic parse max daily downloads?

ChatConfig = {
    server = '',
    canale = '',
    bot = '',
    user_nick = '',
    daily_allowed_downloads = 0,
    download_wait_sec = 0
}

ScriptConfig = {
    log_enabled = true,
    file_log_enabled = true,
    file_log_folder = [[c:\temp\hexchat-download\]],
    file_log_filename = 'hyperfiles.log',
    timer_restart_cmd = '__hyperfiles_restart',
    timer_refnum = 49049
}

function ChatConfig:new(p_server, p_canale, p_max_download, p_wait, p_bot_name)
    self = {}
    self.server = p_server
    self.canale = p_canale
    self.daily_allowed_downloads = p_max_download
    self.download_wait_sec = p_wait
    self.bot = p_bot_name
    self.user_nick = hexchat.get_info('nick')
    return self
end

DownloadStates = {
    none = nil,
    accepting = 1,
    inProgress = 2,
    sleeping = 3
}

DownloadContext = {
    download_index = 0,
    state = DownloadStates.none,
    accept_string = nil,
    finish_string = nil,
    file = nul
}

-- config

UndernetConfig = ChatConfig:new('undernet', '#mp3passion', 100, nil, 'Callabro')
inputFilenamePath = [[c:\temp\hexchat-download\botcallabro]]
fileLogger = nil
hookRegistration = nil

------------------------------

function blog(text)
    if ScriptConfig.log_enabled then
        hexchat.print('[hyperfiles] ' .. text)
    end
    if fileLogger then
        local timestamp = os.date('%H:%M:%S')
        fileLogger:write(timestamp .. ' ' .. text .. "\n")
    end
end

function readInputCommands()
    local downloadCmds = {}
    for downloadLine in io.lines(inputFilenamePath) do
        table.insert(downloadCmds, downloadLine)
    end
    return downloadCmds
end

function commandsHook(words, aggr, userdata)
    xpcall(function() processHookEvent(aggr[1]) end, catch_exception)
end

function processHookEvent(serverEvent)
    local captured = false

    if DownloadContext.state == DownloadStates.none then
        captured = true
        return
    end

    if DownloadContext.state == DownloadStates.accepting then
        captured = true
        serverEvent_ci = string.lower(serverEvent)
        if string.find(serverEvent_ci, DownloadContext.accept_string) then
            blog('download accepted')
            DownloadContext.state = DownloadStates.inProgress
            return
        end
    end

    if DownloadContext.state == DownloadStates.inProgress then
        captured = true

        local transfers = get_transfers_list()
        local dcc = find_transfer_by_filename(transfers, DownloadContext.name)
        if dcc == nil then return end

        if dcc.bytes_transferred == dcc.file_size then
            blog('download finished')
            if (fileLogger) then fileLogger:flush() end
            DownloadContext.state = DownloadStates.sleeping
            if UndernetConfig.download_wait_sec then
                hexchat.command('TIMER -refnum ' .. ScriptConfig.timer_refnum .. ' -repeat 1 ' .. UndernetConfig.download_wait_sec .. ' ' .. ScriptConfig.timer_restart_cmd)
                blog('wait phase')
                return
            end
        end
    end

    if DownloadContext.state == DownloadStates.sleeping then
        captured = true

        if UndernetConfig.download_wait_sec then
            if not string.find(serverEvent, ScriptConfig.timer_restart_cmd) then return end
        end

        if DownloadContext.download_index >= #downloadCmds then
            blog('script end: no more downloads')
            dispose()
            return
        end

        if DownloadContext.download_index >= UndernetConfig.daily_allowed_downloads then
            blog('script end: max daily download reached')
            dispose()
            return
        end

        launchNextDownload()
        return
    end

    if captured == false then blog('error state ' .. DownloadContext.state) end
end

function launchNextDownload()
    DownloadContext.download_index = DownloadContext.download_index + 1
    local filename = downloadCmds[DownloadContext.download_index]
    --TODO loop while #filename <= (#UndernetConfig.bot + 2)
    local cmd = 'MSG ' .. UndernetConfig.canale .. ' !' .. UndernetConfig.bot .. ' ' .. filename
    escFilename = regexEscapeValue(filename)
    escNick = regexEscapeValue(UndernetConfig.user_nick)
    escBot = regexEscapeValue(UndernetConfig.bot)
    DownloadContext.accept_string = string.lower(':' .. escBot .. '.+NOTICE ' .. escNick .. '.+Request Accepted.+' .. escFilename .. '.+Position')
    DownloadContext.finish_string = ':' .. escBot .. '.+NOTICE ' .. escNick .. '.+Sent.+' .. escFilename .. '.+Allowed'

    local dccName, ignore = string.gsub(filename, ' ', '_')
    DownloadContext.name = dccName

    blog('launching ' .. cmd .. ' to ' .. DownloadContext.name)
    DownloadContext.state = DownloadStates.accepting
    hexchat.command(cmd)
end

function regexEscapeValue(text)
    s, l = string.gsub(text, '[%^%$%(%)%%%.%[%]%*%+%-%?]', '%%%1')
    return s
end

function startHyperfiles()
    --TODO DownloadContext.reset()
    DownloadContext.download_index = 0
    DownloadContext.state = DownloadStates.none

    if (ScriptConfig.file_log_enabled) then
        local logfilepath = ScriptConfig.file_log_folder .. ScriptConfig.file_log_filename
        fileLogger = io.open(logfilepath, 'a')
    end
    downloadCmds = readInputCommands()
    cmdsCount = #downloadCmds
    blog(cmdsCount .. ' downloads')
    hookRegistration = hexchat.hook_server(nil, commandsHook)

    blog('starting download')
    launchNextDownload()
end

function dispose()
    if (fileLogger) then
        fileLogger:close()
        fileLogger = nil
    end
    hookRegistration:unhook()
end

function catch_exception(err)
    hexchat.print('[hyperfiles] ERROR ' .. err)
end

DccItem = {
    bytes_per_second = 0,
    file = nil,
    bot_nick = nil,
    bytes_transferred = 0,
    file_size = 0
}

function DccItem:new(p_bytesPerSecond, p_sourceFile, p_botNick, p_bytesTransferred, p_fileSize)
    self = {}
    self.bytes_per_second = p_bytesPerSecond
    self.file = p_sourceFile
    self.bot_nick = p_botNick
    self.bytes_transferred = p_bytesTransferred
    self.file_size = p_fileSize
    return self
end

function get_transfers_list()
    local items = {}
    for item in hexchat.iterate('dcc') do
        local dccItem = DccItem:new(item.cps, item.file, item.nick, item.pos, item.size)
        table.insert(items, dccItem)
    end
    return items
end

function find_transfer_by_filename(p_transfers, p_filename)
    filename_ci = string.lower(p_filename)
    for id = 1, #p_transfers do
        local dcc = p_transfers[id]
        local dccName_ci = string.lower(dcc.file)
        if filename_ci == dccName_ci and dcc.bot_nick == UndernetConfig.bot then
            return p_transfers[id]
        end
    end
    return nil
end

blog('loaded')