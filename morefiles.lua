hexchat.register('downloader', 7, '')
hexchat.print('[morefiles] starting')

-- TODOS
-- cerca contesto con nome room giusta e usa nome bot invece di scrivere comando intero in file input
-- hexchat.iterate(dcc)
-- timer -> text event?
-- extra layer sicurezza per limitare comandi/secondo verso server
-- array -> stack/queue
-- logic->internal event->log, log class
-- bot multipli?
-- timeouts? accept+queue failure, download (se speed < min kb), download aborted
-- reload input file to add downloads
-- automatic parse max daily downloads?

ChatConfig = {
    server = '',
    canale = '',
    bot = '',
    user_nick = '',
    daily_allowed_downloads = 0,
    download_wait_sec = 0
}

ScriptConfig = {
    log_enabled = true,
    file_log_enabled = true,
    file_log_folder = [[c:\temp\hexchat-download\]],
    file_log_filename = 'morefiles.log',
    timer_restart_cmd = '__morefiles_restart',
    timer_refnum = 49049
}

function ChatConfig:new(p_server, p_canale, p_max_download, p_wait, p_bot_name)
    self.server = p_server
    self.canale = p_canale
    self.daily_allowed_downloads = p_max_download
    self.download_wait_sec = p_wait
    self.bot = p_bot_name
    self.user_nick = hexchat.get_info('nick')
    return self
end

DownloadStates = {
    none = nil,
    accepting = 1,
    inProgress = 2,
    sleeping = 3
}

DownloadContext = {
    download_index = 0,
    state = DownloadStates.none,
    accept_string = nil,
    finish_string = nil
}

-- config
UndernetConfig = ChatConfig:new('undernet', '#mp3passion', 300, nil, 'Sonota')
inputFilenamePath = [[c:\temp\hexchat-download\botdownloads]]
fileLogger = nil
hookRegistration = nil

------------------------------

function blog(text)
    if ScriptConfig.log_enabled then
        hexchat.print('[morefiles] ' .. text)
    end
    if fileLogger then
        local timestamp = os.date('%H:%M:%S')
        fileLogger:write(timestamp .. ' ' .. text .. "\n")
    end
end

function readInputCommands()
    local downloadCmds = {}
    for downloadLine in io.lines(inputFilenamePath) do
        table.insert(downloadCmds, downloadLine)
    end
    return downloadCmds
end

function commandsHook(words, aggr, userdata)
    xpcall(function() processHookEvent(aggr[1]) end, catch)
end

function processHookEvent(serverEvent)
    local captured = false

    if DownloadContext.state == DownloadStates.none then
        --if (serverEvent == ScriptConfig.timer_restart_cmd) then startMorefiles() end
        captured = true
        return
    end

    if DownloadContext.state == DownloadStates.accepting then
        captured = true
        if string.find(serverEvent, DownloadContext.accept_string) then
            blog('download accepted')
            DownloadContext.state = DownloadStates.inProgress
            return
        end
    end

    if DownloadContext.state == DownloadStates.inProgress then
        captured = true
        if string.find(serverEvent, DownloadContext.finish_string) then
            blog('download finished')
            if (fileLogger) then fileLogger:flush() end
            DownloadContext.state = DownloadStates.sleeping
            if UndernetConfig.download_wait_sec then
                hexchat.command('TIMER -refnum ' .. ScriptConfig.timer_refnum .. ' -repeat 1 ' .. UndernetConfig.download_wait_sec .. ' ' .. ScriptConfig.timer_restart_cmd)
                blog('wait phase')
                return
            end
        end
    end

    if DownloadContext.state == DownloadStates.sleeping then
        captured = true

        if UndernetConfig.download_wait_sec then
            if not string.find(serverEvent, ScriptConfig.timer_restart_cmd) then return end
        end

        if DownloadContext.download_index >= #downloadCmds then
            blog('script end: no more downloads')
            dispose()
            return
        end

        if DownloadContext.download_index >= UndernetConfig.daily_allowed_downloads then
            blog('script end: max daily download reached')
            dispose()
            return
        end

        launchNextDownload()
        return
    end

    if captured == false then blog('error state ' .. DownloadContext.state) end
end

function launchNextDownload()
    DownloadContext.download_index = DownloadContext.download_index + 1
    local filename = downloadCmds[DownloadContext.download_index]
    --TODO loop while #filename <= (#UndernetConfig.bot + 2)
    local cmd = 'MSG ' .. UndernetConfig.canale .. ' !' .. UndernetConfig.bot .. ' ' .. filename
    escFilename = regexEscapeValue(filename)
    escNick = regexEscapeValue(UndernetConfig.user_nick)
    escBot = regexEscapeValue(UndernetConfig.bot)
    DownloadContext.accept_string = ':' .. escBot .. '.+NOTICE ' .. escNick .. '.+Request Accepted.+' .. escFilename .. '.+Position'
    DownloadContext.finish_string = ':' .. escBot .. '.+NOTICE ' .. escNick .. '.+Sent.+' .. escFilename .. '.+Allowed'
    blog('launching ' .. cmd)
    DownloadContext.state = DownloadStates.accepting
    hexchat.command(cmd)
end

function regexEscapeValue(text)
    s, l = string.gsub(text, '[%^%$%(%)%%%.%[%]%*%+%-%?]', '%%%1')
    return s
end

function startMorefiles()
    --TODO DownloadContext.reset()
    DownloadContext.download_index = 0
    DownloadContext.state = DownloadStates.none

    if (ScriptConfig.file_log_enabled) then
        local logfilepath = ScriptConfig.file_log_folder .. ScriptConfig.file_log_filename
        fileLogger = io.open(logfilepath, 'a')
    end
    downloadCmds = readInputCommands()
    cmdsCount = #downloadCmds
    blog(cmdsCount .. ' downloads')

    if hookRegistration == nil then
        hookRegistration = hexchat.hook_server(nil, commandsHook)
    end
    UndernetConfig.user_nick = hexchat.get_info('nick')
    blog('starting download')
    launchNextDownload()
end

function dispose()
    if (fileLogger) then
        fileLogger:close()
        fileLogger = nil
    end
    hookRegistration:unhook()
end

function catch(err)
    hexchat.print('[morefiles] ERROR ' .. err)
end

blog('loaded')